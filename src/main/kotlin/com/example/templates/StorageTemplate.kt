package com.example.templates

import io.ktor.server.html.*
import kotlinx.html.*

class StorageTemplate(private val name: String?, private val count: Int) : Template<HTML> {
    override fun HTML.apply() {
        head {
            title { +"Storage Selection" }
            link(rel = "icon", href = "/static/logo.png", type="image/png")
            link(rel = "stylesheet", href = "/static/style.css", type = "text/css")
        }
        body {
            p {
                +"Hello, $name! Visit count for this session is $count."
            }
            div(classes = "storage-selection-container") {
                h3 { +"Select storage type:" }
                form(action = "/setstorage", method = FormMethod.post) {
                    div(classes = "form-group") {
                        label {
                            attributes["for"] = "storage"
                            +"Storage: "
                        }
                        select {
                            name = "storage"
                            id = "storage"
                            classes = setOf("form-control")
                            option {
                                value = "memory"
                                +"In-Memory Storage"
                            }
                            option {
                                value = "file"
                                +"File-Based Storage"
                            }
                        }
                    }
                    div(classes = "form-group") {
                        submitInput(classes = "submit-button") {
                            value = "Select"
                        }
                    }
                }
            }
        }
    }
}
