package com.example.data

import io.ktor.server.auth.*

data class UserSession(val name: String, val count: Int, val score: Int = 0, val highScore: Int = 0, val lives: Int = 3) : Principal
