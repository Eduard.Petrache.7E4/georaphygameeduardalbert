package com.example.data

import com.example.countryData.Country
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File

class FileStorage(filename: String) : Storage {
    private val countries = mutableListOf<String>()

    init {
        val json = File(filename).readText()
        val countriesData = Json.decodeFromString<List<Country>>(json)
        countries.addAll(countriesData.map { it.name.common })
    }

    override fun getCountries(): List<String> {
        return countries
    }
}