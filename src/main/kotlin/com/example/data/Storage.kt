package com.example.data

interface Storage {
    fun getCountries(): List<String>
}