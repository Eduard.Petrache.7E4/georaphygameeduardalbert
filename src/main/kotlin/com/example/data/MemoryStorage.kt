package com.example.data

class MemoryStorage : Storage {
    private val countries = mutableListOf<String>()


    override fun getCountries(): List<String> {
        return countries
    }
}