package com.example.routing

// Ktor imports
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.html.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.sessions.*
import io.ktor.server.request.*
import io.ktor.util.*

// Serialization and HTTP imports
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.request.get
import io.ktor.client.statement.*
import java.io.File

// Application-specific imports
import com.example.countryData.Country
import com.example.data.FileStorage
import com.example.data.MemoryStorage
import com.example.data.Storage
import com.example.data.UserSession
import com.example.templates.GameTemplate
import com.example.templates.LoginTemplate
import com.example.templates.StorageTemplate

fun Route.routesKtor() {
    val userCounter = mutableMapOf<String, Int>()

    // Root route
    get("/") {
        val userSession = call.sessions.get<UserSession>()
        if (userSession != null) {
            // The user is logged in
            call.respondRedirect("/storage")
        } else {
            // The user is not logged in
            call.respondRedirect("/login")
        }
    }

    // Login routes
    get("/login") {
        call.respondHtmlTemplate(LoginTemplate()) {}
    }

    authenticate("auth-form") {
        post("/login") {
            val userName = call.principal<UserIdPrincipal>()?.name.toString()
            val userCount = userCounter[userName] ?: 1
            call.sessions.set(UserSession(name = userName, count = userCount, score = 0, highScore = 0, lives = 3))
            call.respondRedirect("/storage")
        }
    }

    // Storage routes
    authenticate("auth-session") {
        get("/storage") {
            val userSession = call.principal<UserSession>()
            val updatedCount = userSession!!.count + 1
            userCounter[userSession.name] = updatedCount
            call.sessions.set(userSession.copy(count = updatedCount))
            call.respondHtmlTemplate(StorageTemplate(userSession.name, updatedCount)) {}
        }

        post("/setstorage") {
            val storageType = call.receiveParameters()["storage"]
            val storage: Storage = when (storageType) {
                "memory" -> MemoryStorage()
                "file" -> FileStorage("countries.txt")
                else -> throw IllegalArgumentException("Invalid storage type")
            }
            call.application.attributes.put(AttributeKey("storage"), storage)
            call.respondRedirect("/game")
        }
    }

    // Game routes
    authenticate("auth-session") {
        get("/game") {
            val userSession = call.principal<UserSession>()
            val storage = call.application.attributes[AttributeKey<Storage>("storage")]

            val jsonConfig = Json { ignoreUnknownKeys = true }

            // If storage type is file, read countries from file instead of making API request
            val countries = if (storage is FileStorage) {
                val json = File("countries.txt").readText()
                jsonConfig.decodeFromString<List<Country>>(json)
            } else {
                val httpClient = HttpClient(Apache)
                val url = "https://restcountries.com/v3.1/all?fields=name,flags"
                val response: HttpResponse = httpClient.use { it.get(url) }
                val responseBody: String = response.bodyAsText()
                jsonConfig.decodeFromString<List<Country>>(responseBody)
            }


            val randomCountry = countries.random()

            val message = when (call.parameters["result"]) {
                "correct" -> "Correct! The country was ${call.parameters["country"]}."
                "incorrect" -> "Incorrect. The correct answer was ${call.parameters["country"]}."
                "gameover" -> "Game over. The correct answer was ${call.parameters["country"]}. You have run out of lives."
                else -> null
            }

            call.respondHtmlTemplate(
                GameTemplate(
                    country = randomCountry.name.common,
                    flagUrl = randomCountry.flags.png,
                    message = message,
                    score = userSession!!.score,
                    highScore = userSession.highScore,
                    lives = userSession.lives
                )
            ) {}
        }

        post("/game") {
            val userAnswer = call.receiveParameters()["country"]
            val correctAnswer = call.parameters["country"]
            val userSession = call.principal<UserSession>()

            if (userAnswer != null && userAnswer == correctAnswer) {
                val newScore = userSession!!.score + 1
                val newHighScore = maxOf(userSession.highScore, newScore)
                call.sessions.set(userSession.copy(score = newScore, highScore = newHighScore))
                call.respondRedirect("/game?result=correct&country=$correctAnswer")
            } else {
                val remainingLives = userSession!!.lives - 1
                if (remainingLives == 0) {
                    call.sessions.set(userSession.copy(score = 0, lives = 3))
                    call.respondRedirect("/game?result=gameover&country=$correctAnswer")
                } else {
                    call.sessions.set(userSession.copy(lives = remainingLives))
                    call.respondRedirect("/game?result=incorrect&country=$correctAnswer")
                }
            }
        }
    }
}


