package com.example.countryData

import kotlinx.serialization.Serializable

@Serializable
data class CountryNativeName(
    val official: String,
    val common: String
)