package com.example.countryData

import kotlinx.serialization.Serializable

@Serializable
data class CountryFlags(
    val png: String,
    val svg: String,
    val alt: String
)
