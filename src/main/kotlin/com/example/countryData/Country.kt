package com.example.countryData

import kotlinx.serialization.Serializable

@Serializable
data class Country(
    val name: CountryName,
    val flags: CountryFlags
)
