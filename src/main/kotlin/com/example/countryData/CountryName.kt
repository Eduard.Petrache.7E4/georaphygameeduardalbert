package com.example.countryData

import kotlinx.serialization.Serializable

@Serializable
data class CountryName(
    val common: String,
    val official: String,
    val nativeName: Map<String, CountryNativeName>
)
